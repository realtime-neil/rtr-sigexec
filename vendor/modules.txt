# github.com/ProtonMail/go-crypto v0.0.0-20210920160938-87db9fbc61c7
github.com/ProtonMail/go-crypto/bitcurves
github.com/ProtonMail/go-crypto/brainpool
github.com/ProtonMail/go-crypto/eax
github.com/ProtonMail/go-crypto/internal/byteutil
github.com/ProtonMail/go-crypto/ocb
github.com/ProtonMail/go-crypto/openpgp
github.com/ProtonMail/go-crypto/openpgp/aes/keywrap
github.com/ProtonMail/go-crypto/openpgp/armor
github.com/ProtonMail/go-crypto/openpgp/clearsign
github.com/ProtonMail/go-crypto/openpgp/ecdh
github.com/ProtonMail/go-crypto/openpgp/elgamal
github.com/ProtonMail/go-crypto/openpgp/errors
github.com/ProtonMail/go-crypto/openpgp/internal/algorithm
github.com/ProtonMail/go-crypto/openpgp/internal/ecc
github.com/ProtonMail/go-crypto/openpgp/internal/encoding
github.com/ProtonMail/go-crypto/openpgp/packet
github.com/ProtonMail/go-crypto/openpgp/s2k
# github.com/ProtonMail/go-mime v0.0.0-20190923161245-9b5a4261663a
github.com/ProtonMail/go-mime
# github.com/ProtonMail/gopenpgp/v2 v2.2.4
github.com/ProtonMail/gopenpgp/v2/armor
github.com/ProtonMail/gopenpgp/v2/constants
github.com/ProtonMail/gopenpgp/v2/crypto
github.com/ProtonMail/gopenpgp/v2/internal
# github.com/jessevdk/go-flags v1.5.0
github.com/jessevdk/go-flags
# github.com/pkg/errors v0.9.1
github.com/pkg/errors
# github.com/sirupsen/logrus v1.8.1
github.com/sirupsen/logrus
# golang.org/x/crypto v0.0.0-20210921155107-089bfa567519
golang.org/x/crypto/cast5
golang.org/x/crypto/curve25519
golang.org/x/crypto/curve25519/internal/field
golang.org/x/crypto/ed25519
golang.org/x/crypto/ed25519/internal/edwards25519
# golang.org/x/sys v0.0.0-20211020174200-9d6173849985
golang.org/x/sys/internal/unsafeheader
golang.org/x/sys/unix
golang.org/x/sys/windows
# golang.org/x/text v0.3.7
golang.org/x/text/encoding
golang.org/x/text/encoding/charmap
golang.org/x/text/encoding/htmlindex
golang.org/x/text/encoding/internal
golang.org/x/text/encoding/internal/identifier
golang.org/x/text/encoding/japanese
golang.org/x/text/encoding/korean
golang.org/x/text/encoding/simplifiedchinese
golang.org/x/text/encoding/traditionalchinese
golang.org/x/text/encoding/unicode
golang.org/x/text/internal/language
golang.org/x/text/internal/language/compact
golang.org/x/text/internal/tag
golang.org/x/text/internal/utf8internal
golang.org/x/text/language
golang.org/x/text/runes
golang.org/x/text/transform
